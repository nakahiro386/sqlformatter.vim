" sqlformatter.vim

if exists("g:loaded_sqlformatter")
  finish
endif
let g:loaded_sqlformatter = 1

let s:save_cpo = &cpo
set cpo&vim

function! s:Warning(msg) "{{{
  execute 'echohl WarningMsg'
  execute 'echomsg '.string('[sqlformatter.vim] '.a:msg)
  execute 'echohl None'
endfunction "}}}

function! s:Check() "{{{
  if !exists('g:sqlformatter_dir')
    call s:Warning('Please set g:sqlformatter_dir')
    return 1
  endif

  if !exists('g:sqlformatter_cmd')
    call s:Warning('Please set g:sqlformatter_cmd')
    return 1
  endif

  if !isdirectory(g:sqlformatter_dir)
    call s:Warning(printf('{%s} is not directory.', g:sqlformatter_dir))
    return 1
  endif

  if !filereadable(g:sqlformatter_cmd)
    call s:Warning(printf('{%s} not readable.', g:sqlformatter_cmd))
    return
  endif

  if !executable('ruby')
    call s:Warning(printf('{%s} not executable.', 'ruby'))
    return 1
  endif

  return 0
endfunction "}}}

function! s:GetCommand() "{{{
  return join(['ruby -I',
      \ printf('"%s"', g:sqlformatter_dir),
      \ printf('"%s"', g:sqlformatter_cmd)])
endfunction "}}}

function! s:GetInput(line1,line2) "{{{
  return join(getline(a:line1, a:line2), "\n")
endfunction "}}}

function! s:SqlformatterFunc(line1,line2) range "{{{

  if s:Check()
    return
  endif

  let l:cmd = s:GetCommand()
  let l:input = s:GetInput(a:line1,a:line2)

  let l:V = vital#of('Sqlformatter')
  call l:V.load('Process')
  let l:result = l:V.Process.system(l:cmd, {'input' : l:input})
  let l:status = l:V.Process.get_last_status()
  if type(l:status) is type('') && l:status isnot '0'
      \ || type(l:status) is type(0) && l:status isnot 0
    call s:Warning(printf('"[sqlformatter.vim] Return code is %s. command:%s"', l:status, l:cmd))
    return
  endif

  silent execute a:line1 . ',' . a:line2 . 'del _'
  silent execute string(a:line1 - 1) . 'put =l:result'
endfunction "}}}

command! -range=% Sqlformatter call s:SqlformatterFunc(<line1>, <line2>)

noremap <silent> <Plug>(Sqlformatter) :Sqlformatter<CR>

let &cpo = s:save_cpo
unlet s:save_cpo

" vim:foldmethod=marker
